RTD2660H
VGA/VIDEO/HDMI转LVDS/TTL屏接口IC

1、可根据实际情况对35738597RTD2660_V025_0826版本程序作修改。
2、下载方式：
    a）直接对SPI Flash编程
    b）通过Tools ISP的方式编程
3、芯片内核是51的，可以直接用MDK软件打开编译
4、百度网盘里有很多已编译好的固件

这里还有相关的文档及程序：
https://github.com/gamelaster/RTD2662.git

2018.01.18
RTD2662只支持HDMI1.1版本，最高支持到1080i，而非1080p。分辨率支持有限。1366*768下，兼容性不好。